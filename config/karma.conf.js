module.exports = function (config) {
    config.set({

        // base path, that will be used to resolve files and exclude
        basePath: '../',

        // list of files / patterns to load in the browser
        files: [
            'app/bower_components/angular/angular.min.js',
            'app/bower_components/angular-mocks/angular-mocks.js',
            'test/**/*.js',
            'app/js/**/*.js'
        ],

        exclude: [
            'app/js/app.js'
        ],

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,

        // frameworks to use
        frameworks: ['jasmine'],

        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        browsers: ['Chrome'],

        plugins: [
            'karma-jasmine',
            'karma-chrome-launcher',
            'karma-junit-reporter'
        ],

        // test results reporter to use
        // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
        reporters: ['progress', 'junit'],

        junitReporter: {
            // will be resolved to basePath (in the same way as files/exclude patterns)
            outputFile: 'test-results.xml'
        },

        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO
    });
};

/**
 * Created by Wouter.Seyen on 21/05/2014.
 */

angular.module('ae-cafe.services', ['LocalStorageModule']).
    service('MenuService', function ($http) {
        var menuService = {};
        var baseUrl = 'http://localhost:1337/items/';

        menuService.getShortUrl = function (url) {
            return url.replace(baseUrl, '');
        };

        menuService.getMenu = function (url) {
            if (url) {
                url = baseUrl + url;
            }
            else {
                url = baseUrl;
            }
            return $http({
                url: url
            })
        };

        return menuService;
    }).
    service('CartService', function ($http, $rootScope, localStorageService) {
        var cartService = {};

        var cart = localStorageService.get('cart');
        cart = cart ? cart : [];

        cartService.addItem = function (menuItem) {
            if (cart[menuItem.id]) {
                cart[menuItem.id].amount++;
            }
            else {
                cart[menuItem.id] = {
                    amount: 1,
                    item: menuItem
                }
            }
            $rootScope.$broadcast('cartItemAdded', menuItem);
            localStorageService.set('cart', cart);
        };

        cartService.totalItems = function () {
            var items = 0;
            cart.forEach(function (item) {
                items = item ? items + item.amount : items;
            });
            return items;
        };

        cartService.getAllItems = function () {
            // return regular array;
            return _.filter(cart, function (item) {
                return item != null;
            });
        };

        cartService.clear = function () {
            cart = [];
            localStorageService.set('cart', cart);
            $rootScope.$broadcast('cartCleared');
        };

        cartService.postOrder = function () {
            var order = [];
            var items = _.filter(cart, function (cartItem) {
                return cartItem != null;
            });
            items.forEach(function (item) {
                for (var i = 0; i < item.amount; i++) {
                    order.push(item.item.id);
                }
            });
            return $http({
                url: 'http://localhost:1337/orders',
                method: 'POST',
                data: {
                    items: order
                }
            });
        };

        return cartService;
    })
    .service('OrderService', function($http) {
        var orderService = {};

        orderService.getOrders = function() {
            return $http({
                url: 'http://localhost:1337/orders'
            });
        };

        return orderService;
    });
angular.module('app')
    .service('ShoppingCartService', function($rootScope) {
        var self = this;
        var items = [];

        self.addItem = function(item) {
            items.push(item);
            $rootScope.$broadcast('itemAdded', item);
        };

        self.getNbOfItems = function() {
            return items.length;
        };

        return self;
    })
    .controller('MenuDetailsController', function($scope, ShoppingCartService) {
        $scope.order = function(item) {
            ShoppingCartService.addItem(item);
        }
    })
    .directive('menuDetails', function () {
    return {
        templateUrl: 'partials/menuDetails.html',
        restrict: 'A',
        scope: {
            item: '='
        },
        controller: 'MenuDetailsController'
    }
});
angular.module('ae-cafe')
    .controller('MenuItemController', function($location, $scope) {
        $scope.isActive = function(url) {
            return $location.path() == url;
        }
    })
    .directive('menuItem', function() {
    return {
        template: '<li ng-class="{ active: isActive(\'{{url}}\')}"><a href="#{{url}}">{{text}}</a></li>',
        scope: {
            text: '@',
            url: '@'
        },
        restrict: 'E',
        replace: true,
        controller: 'MenuItemController'
    }
});
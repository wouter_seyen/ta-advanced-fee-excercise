angular.module('ae-cafe.controllers', [])
    .controller('MenuController', function ($scope, $routeParams, $location, MenuService) {
        $scope.menu = [];
        $scope.parent = null;
        $scope.title = null;
        $scope.item = null;

        var self = this;

        $scope.navParent = function (url) {
            url = 'menu/' + MenuService.getShortUrl(url);
            $location.path(url);
        };

        $scope.navMenu = function (menuItem) {
            if (menuItem._links.children) {
                var url = 'menu/' + MenuService.getShortUrl(menuItem._links.children.href);
                $location.path(url);
            }
            else {
                self.navMenuContents(menuItem._links.self.href);
            }
        };

        this.navMenu = function () {
            var url = $routeParams.menuItem;
            MenuService.getMenu(url).success(function (response) {
                $scope.menu = response._embedded.items;
                $scope.parent = response._links.parent ? response._links.parent.href : null;
                $scope.title = response._links.parent ? response.title : 'Menu';
            });
        };

        this.navMenuContents = function (url) {
            MenuService.getMenu(MenuService.getShortUrl(url)).success(function (response) {
                $scope.item = response
            });
        };

        this.navMenu();
    })
    .directive('menuItemDetails', function () {
        return {
            scope: {
                item: '='
            },
            controller: 'MenuItemDetailsController',
            templateUrl: 'partials/menuItemDetails.html'
        }
    })
    .controller('MenuItemDetailsController', function ($scope, CartService) {
        $scope.addToCart = function (foodItem) {
            CartService.addItem(foodItem);
        };
    })
    .controller('NavigationController', function ($scope, CartService) {
        $scope.items = CartService.totalItems();

        $scope.$on('cartItemAdded', function () {
            $scope.items = CartService.totalItems();
        });

        $scope.$on('cartCleared', function () {
            $scope.items = CartService.totalItems();
        });
    }).
    controller('CartController', function ($scope, CartService) {
        $scope.cart = CartService.getAllItems();

        $scope.totalValue = function () {
            var sum = 0;
            $scope.cart.forEach(function (cartItem) {
                sum += cartItem.amount * cartItem.item.price;
            });
            return sum;
        };

        $scope.clearBasket = function () {
            CartService.clear();
        };

        $scope.postOrder = function () {
            CartService.postOrder().success(function (data, status, headers, config, statusText) {
                CartService.clear();
            }).error(function (data, status, headers, config, statusText) {
                alert('ai, \'t is mis');
            });
        };

        $scope.$on('cartCleared', function () {
            $scope.cart = CartService.getAllItems();
        });
    })
    .controller('OrderController', function ($scope, OrderService) {
        $scope.orders = [];

        var self = this;
        $scope.getOrders = function() {
            self.getOrders();
        };

        this.getOrders = function () {
            $scope.orders = [];
            OrderService.getOrders().success(function (response) {
                $scope.orders = response._embedded.items;
            });
        };

        $scope.toggle = function(order) {
            if (order.opened) {
                order.opened = false;
            }
            else {
                order.opened = true;
            }
        };

        this.getOrders();
    });
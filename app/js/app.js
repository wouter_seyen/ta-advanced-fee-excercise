angular.module('ae-cafe', [
    'ae-cafe.services',
    'ae-cafe.controllers',
    'ngRoute'
])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/home', {templateUrl: 'partials/home.html'})
            .when('/menu', {templateUrl: 'partials/menu.html', controller: 'MenuController'})
            .when('/menu/:menuItem*', {templateUrl: 'partials/menu.html', controller: 'MenuController'})
            .when('/shoppingCart', {templateUrl: 'partials/cartDetails.html', controller: 'CartController'})
            .when('/orders', {templateUrl: 'partials/orders.html', controller: 'OrderController'})
            .when('/orders/:id', {templateUrl: 'partials/order.html', controller: 'OrderController'})
            .otherwise({redirectTo: '/home'});
    });